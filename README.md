# Projet CV

## Objectifs

* Maquetter une page web
* Réaliser une interface utilisateur web statique et adaptable
* Intégrer une page web à partir d'un wireframe
* Utiliser un framework CSS

## Modalités

* Projet **individuel**
* 1 semaine
* Code versionné

## Tâches

Réaliser une page pour présenter votre CV en utilisant un framework CSS pour simplifier l'implémentation de la partie responsive.

1. Réaliser les wireframes (desktop + mobile)
2. Proposer un planning pour organiser son travail
3. Sélectionner un framework CSS
4. Créer un dépôt Git pour versionner votre code
5. Débuter l'intégration de votre page
