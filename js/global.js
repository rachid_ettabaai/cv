$(document).ready(function (e) {

    //Modification du background-color des parties header et footer
    $("header,footer").css({
        "background-color": "#f4f5f6"
    })
    ///////////////////////////////////////////////////////////////

    //Ajout du padding pour espacer le contenu des côtes///////////
    $("header,footer,h2,article").css({
        "padding": "2rem"
        
    })
    ///////////////////////////////////////////////////////////////

    //Une barre de navigation simple pour le framework CSS Milligram 
    $(".navbar_in").css({
        "margin-bottom": "10px"
    })

    $(".navbar_in ul").css({
        "list-style-type": "none",
        "margin": "0",
        "padding": "0",
        "overflow": "hidden",
        "background-color": "#606c76"
    })

    $(".navbar_in li").css({
        "float": "left",
        "margin-bottom": "0"
    })

    $(".navbar_in li a").css({
        "display": "flex",
        "color": "white",
        "text-align": "center",
        "padding": "14px 16px",
        "text-decoration": "underline"
    })

    $(".navbar_in li a").hover(function(){
        $(this).css({
            "background-color": "#ab5dda",
            "cursor": "pointer"
        })
    }, function(){
        $(this).css({
            "background-color": "#606c76",
            "cursor": "none"
        })
    })
    ///////////////////////////////////////////////////////////////

    //Souligner les titres/////////////////////////////////////////
    $(".title").css({
        "text-decoration": "underline dotted"
    })
    ///////////////////////////////////////////////////////////////

    //Changer la forme du curseur sur la balise abbr///////////////
    $("abbr").css({
        "cursor": "help"
    })
    ///////////////////////////////////////////////////////////////

    //Taille de la map sur la page contact/////////////////////////
    $("#map").css({
        "height": "400px",
        "width": "600px"
    })
    ///////////////////////////////////////////////////////////////

});